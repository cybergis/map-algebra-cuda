#include "parser.h"
#include <tclap/CmdLine.h>



using std::string;

CmdArgs ParseArgs(int argc, char **argv){
    CmdArgs cmd_args;

    try {
        TCLAP::CmdLine cmd("Map Algebra tool using CUDA", ' ', "0.1");
        TCLAP::UnlabeledValueArg<string> op_arg("op", "operator", true, "", "operator");
        cmd.add(op_arg);
        TCLAP::UnlabeledValueArg<string> input_arg_1("i1", "input tif 1", true, "", "input1");
        cmd.add(input_arg_1);
        TCLAP::UnlabeledValueArg<string> input_arg_2("i2", "input tif 2", true, "", "input2");
        cmd.add(input_arg_2);
        TCLAP::UnlabeledValueArg<string> output_arg("o", "output tif", true, "", "output");
        cmd.add(output_arg);
        TCLAP::SwitchArg debug_arg ("v", "verbo", "print verbo information", cmd, false);
        TCLAP::ValueArg<int> band_arg_1 ("", "band1", "the band for input 1", false, 1, "band1");
        cmd.add(band_arg_1);
        TCLAP::ValueArg<int> band_arg_2 ("", "band2", "the band for input 2", false, 1, "band2");
        cmd.add(band_arg_2);

        cmd.parse(argc, argv);

        cmd_args.op = op_arg.getValue();
        cmd_args.is_debug = debug_arg.getValue();
        cmd_args.input_1 = input_arg_1.getValue();
        cmd_args.input_2 = input_arg_2.getValue();
        cmd_args.output = output_arg.getValue();
        //cmd_args.band1 = band_arg_1.getValue();
        cmd_args.band2 = band_arg_2.getValue();
    } catch (TCLAP::ArgException &e)  // catch any exceptions
        { std::cerr << "error: " << e.error() << " for arg " << e.argId() << std::endl; }
    return cmd_args; 
}



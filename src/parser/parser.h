#ifndef PARSER_H
#define PARSER_H

#include <string>

struct CmdArgs{
    bool is_debug;
    std::string op;
    std::string input_1;
    std::string input_2;
    std::string output;
    int band1;
    int band2;
};

CmdArgs ParseArgs(int argc, char **argv);

#endif

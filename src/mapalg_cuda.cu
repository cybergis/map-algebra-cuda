#include <cuda.h>
#include <cuda_runtime_api.h>
#include <vector_types.h>
#include <ctime>
#include <cmath>

#include "parser.h"
#include "data.h"


using std::string;

struct RasterInfo{
    GDALDatasetH r_in;
    float *data;
    double geo_ref[6];
    char prj[2048];
    float nodata;
    int dim_x;
    int dim_y;

    RasterInfo(string filename){
        double nodata_tmp;
        r_in = raster_open(filename.c_str(), geo_ref, prj, 
                &nodata_tmp, &dim_x, &dim_y);
        nodata = (float) nodata_tmp;
        if (r_in == NULL)
            return;
        data = (float *) malloc(dim_x * dim_y * sizeof(float));
        data = raster_read(r_in, 0, 0, dim_x, dim_y);
    }

    ~RasterInfo(){
        raster_close(r_in);
        free(data);
    }
    
    int size(){
        if (r_in == NULL)
            return 0;
        return dim_x * dim_y;
    }

    void PrintInfo(){
        if(r_in == NULL)
            printf("The raster is not read correctly\n");
        else
            print_raster_info(r_in, geo_ref, prj, nodata, dim_x, dim_y);
    }
};
__host__
inline void cudaCheckError(cudaError_t e) {
    if(e != cudaSuccess) {
        fprintf(stderr,"%s in %s at line %d\n",
                        cudaGetErrorString(e),
                        __FILE__, __LINE__);
        exit(EXIT_FAILURE);
    }
}

__global__
void binary_op( float *in_raster1, float *in_raster2, float *out_raster, 
        int op, int dim_x, int dim_y, float nodata1, float nodata2, float nodata_out) {

    // Calculate the thread's index
    int x = threadIdx.x + (blockIdx.x * blockDim.x);
    int y = threadIdx.y + (blockIdx.y * blockDim.y);

    // Calculate what index of the raster the thread will perform on
    int raster_idx = x + (dim_x * y);

    double threshold = 0.001;
    if (fabsf((in_raster1[raster_idx]) - nodata1) < threshold
            ||  fabs((double)(in_raster2[raster_idx]) - nodata2) < threshold){
        out_raster[raster_idx] = nodata_out;
        return;
    }

    // Calculate and store the sum
    switch(op){
        case 1:
            out_raster[raster_idx] = in_raster1[raster_idx] + in_raster2[raster_idx]; break;
        case 2:
            out_raster[raster_idx] = in_raster1[raster_idx] - in_raster2[raster_idx]; break;
        case 3:
            out_raster[raster_idx] = in_raster1[raster_idx] * in_raster2[raster_idx]; break;
        case 4:
            if (in_raster2[raster_idx] != 0)
                out_raster[raster_idx] = in_raster1[raster_idx] / in_raster2[raster_idx];
            else
                out_raster[raster_idx] = nodata_out;
            break;
    }
}

class CudaPipline{
    private:
        float *current;
        float *input;
        float *output;
        dim3 block_dim;
        dim3 grid_dim;
        int size_in_byte;
        float nodata2;
    public:
        int dim_x;
        int dim_y;
        float nodata1;
   
    public: 
        CudaPipline(RasterInfo& r){
            clock_t begin = clock(); 

            size_in_byte = r.size() * sizeof(float);
            dim_x = r.dim_x;
            dim_y = r.dim_y;
            nodata1 = r.nodata;
            
            cudaCheckError( cudaMalloc((void **) &current, size_in_byte) );
            input = NULL;
            cudaCheckError( cudaMalloc((void **) &output, size_in_byte) );
            cudaCheckError( cudaMemcpy(current, r.data, size_in_byte, cudaMemcpyHostToDevice) );
           
            block_dim.x = block_dim.y = 32;
            block_dim.z = 1;
            grid_dim.x = (dim_x-1) / 32 + 1;
            grid_dim.y = (dim_y-1) / 32 + 1;
            grid_dim.y = 1;

            clock_t end = clock();
            double elapsed_secs = double(end - begin) / CLOCKS_PER_SEC;
            printf("Cuda set up done, %f secs\n", elapsed_secs);
        }

        ~CudaPipline(){
            cudaCheckError( cudaFree((void*) current) );
            cudaCheckError( cudaFree((void*) input) );
            cudaCheckError( cudaFree((void*) output) );
        }
       
        bool addInput(RasterInfo& r){
            if (input == NULL)
                cudaCheckError( cudaMalloc((void **) &input, size_in_byte) );

            if (r.dim_x != dim_x || r.dim_y != dim_y){
                fprintf(stderr, "Error: raster size are not equal, cannot compute\n");
                return false;
            }
            cudaCheckError( cudaMemcpy(input, r.data, size_in_byte, cudaMemcpyHostToDevice) );
            nodata2 = r.nodata;
            return true;
        }
        void BinOp(int op_num, float nodata){
            clock_t begin = clock();
            binary_op<<<grid_dim, block_dim>>>( current, input, output, 
                    op_num, dim_x, dim_y, 
                    nodata1, nodata2, nodata);

            // swap current and output, making result always in current
            float *tmp = current;
            current = output;
            output = tmp;
            cudaDeviceSynchronize();

            clock_t end = clock();
            double elapsed_secs = double(end - begin) / CLOCKS_PER_SEC;
            printf("Cuda binary operation done, %f secs\n", elapsed_secs);
        }
        void MonOp(int op_num, float nodata){
            
        }

        /**
         * Malloc a memory for the output, need to manually free the result
         */
        float *GetOutput(){
            float *out = (float*) malloc(size_in_byte); 
            cudaCheckError( cudaMemcpy(out, current, size_in_byte, 
                        cudaMemcpyDeviceToHost));
            return out;
        }
    


};

int main(int argc, char *argv[]){
  
    CmdArgs cmd_args;
    cmd_args = ParseArgs(argc, argv);
 
    // Open first raster file
    RasterInfo r1(cmd_args.input_1);
    printf("----INPUT RASTER 1----\n");
    r1.PrintInfo();

    // Open first raster file
    RasterInfo r2(cmd_args.input_2);
    printf("----INPUT RASTER 2----\n");
    r2.PrintInfo();

    CudaPipline cuda(r1); 
    cuda.addInput(r2);
    cuda.BinOp(1, r1.nodata);
    float *output = cuda.GetOutput();
    free(output); 
   
    GDALDatasetH r_out = raster_create(cmd_args.output.c_str(), cuda.dim_x, cuda.dim_y, r1.geo_ref, r1.prj, cuda.nodata1);
    if (r_out == NULL){
        fprintf(stderr, "Error: Cannot creat output file\n");
        exit(-1);
    }
    raster_write(r_out, cuda.GetOutput(), 0, 0, cuda.dim_x, cuda.dim_y);

        

    return 0;
}
